# movie-theater

This project assumes you have the following python packages installed:

**pandas // matplotlib // mplcursors**

Also assumes that all csv files are inside the root folder, if not download them from: https://www.kaggle.com/datasets/rounakbanik/the-movies-dataset/data

Note: Normally I would use devcontainer to simplify deployment and development across different OS systems.

### If you had a movie theater and could put the movies you wanted, what would they be and why? You can base your decision on other CSV files of the dataset if you like.

I would display the highest rated movie of each year. Also added this information in the tooltip of the chart.