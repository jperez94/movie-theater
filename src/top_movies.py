import pandas as pd
import matplotlib.pyplot as plt
import os
import mplcursors


# data read
csv_file_path = os.path.join(os.path.dirname(__file__), '../movies_metadata.csv')
movies_df = pd.read_csv(csv_file_path, low_memory=False)
movies_df['release_date'] = pd.to_datetime(movies_df['release_date'], errors='coerce')
movies_df['release_year'] = movies_df['release_date'].dt.year

# budget to numeric, coercing errors to NaN
movies_df['budget'] = pd.to_numeric(movies_df['budget'], errors='coerce')

# Removal of rows where 'budget' is NaN or 0
movies_df = movies_df[(movies_df['release_year'] >= 1975) & (movies_df['budget'].notna()) & (movies_df['budget'] > 0)]
average_budget_per_year = movies_df.groupby('release_year')['budget'].mean()
top_3_budgets_per_year = movies_df.sort_values(by='budget', ascending=False).groupby('release_year').head(3)[['release_year', 'budget', 'title']]

# Load ratings data for tooltip
ratings_csv_path = os.path.join(os.path.dirname(__file__), '../ratings.csv')
ratings_df = pd.read_csv(ratings_csv_path)


############################################
# Compute average rating per movie (movieId)
# data inside the ratings csv is at user level

avg_rating_per_movie = ratings_df.groupby('movieId')['rating'].mean().reset_index()
avg_rating_per_movie.columns = ['id', 'avg_rating']

try:
    movies_df['id'] = movies_df['id'].astype(int)
except ValueError:
    pass 

# Merge metadata with rating
movies_df = pd.merge(movies_df, avg_rating_per_movie, on='id', how='left')
movies_df_for_top_rated = movies_df.dropna(subset=['avg_rating']).copy()
top_rated_movies_per_year = movies_df_for_top_rated.loc[movies_df_for_top_rated.groupby('release_year')['avg_rating'].idxmax()][['release_year', 'avg_rating', 'title']]

num_movies_with_budget_above_zero = movies_df.groupby('release_year').size()


######################################################################
# Identify years with lower data reliability compared to the next year
# I.e 2018 has only 2 movies with a budget informed which creates
# unrealiable information for the computation
######################################################################

years_above_threshold = num_movies_with_budget_above_zero[num_movies_with_budget_above_zero > 10].index

average_budget_per_year = average_budget_per_year.loc[years_above_threshold]
top_3_budgets_per_year = top_3_budgets_per_year[top_3_budgets_per_year['release_year'].isin(years_above_threshold)]

min_movies_threshold = 0.5

years_with_low_reliability = []


for i in range(len(average_budget_per_year) - 1):
    year = average_budget_per_year.index[i]
    next_year = average_budget_per_year.index[i + 1]
    if num_movies_with_budget_above_zero[year] < min_movies_threshold * num_movies_with_budget_above_zero[next_year]:
        years_with_low_reliability.append(year)

plt.figure(figsize=(14, 7))

line, = plt.plot(average_budget_per_year.index, average_budget_per_year.values * 10**-6, linestyle='-', color='black', label='Average Budget')
dots = plt.scatter(average_budget_per_year.index, average_budget_per_year.values * 10**-6, color='red')

plt.title('Average Movie Budget Per Year')
plt.xlabel('Year')
plt.ylabel('Average Budget ($M)')
plt.legend()  
plt.grid(True)

cursor = mplcursors.cursor(dots, hover=True)

def format_tooltip_text(year, avg_budget, top_3, max_rating, top_movie_title, num_movies_above_zero):
    avg_budget_million = avg_budget * 1e-6
    top_3_str = "\n".join([f"{row['title']}: ${row['budget']:,.2f}" for _, row in top_3.iterrows()])
    max_rating_formatted = f"{max_rating:.1f}" if isinstance(max_rating, float) else max_rating
    return f'Year: {year}\nAvg Budget: ${avg_budget_million:.2f}M\nMax Rating: {max_rating_formatted} - {top_movie_title}\nTop 3 Budgets:\n{top_3_str}'


@cursor.connect("add")
def on_add(sel):
    year = int(sel.target[0])
    avg_budget = average_budget_per_year.loc[year]  
    top_3_year = top_3_budgets_per_year[top_3_budgets_per_year['release_year'] == year] 
    num_movies_above_zero = num_movies_with_budget_above_zero.loc[year]
    
    if year in top_rated_movies_per_year['release_year'].unique():
        max_rating_entry = top_rated_movies_per_year.loc[top_rated_movies_per_year['release_year'] == year].iloc[0]
        max_rating = max_rating_entry['avg_rating']  
        top_movie_title = max_rating_entry['title']
    else:
        max_rating = 'N/A' 
        top_movie_title = 'N/A'
    
    sel.annotation.set(text=format_tooltip_text(year, avg_budget, top_3_year, max_rating, top_movie_title, num_movies_above_zero))


plt.show()
